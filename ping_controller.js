/*
  Controller dedicated to monitor "ping" tool:
   "Ping" measures the round-trip time for messages sent from the originating host to a destination computer that are echoed back to the source.
*/
const grpc = require('grpc');
const nw_proto = grpc.load('./proto/network_monitor.proto');

/*
  Write output log to InfluxDB
  Currently commented
*/
// const Influx = require('influx');
// const influx = new Influx.InfluxDB({
//  host: 'localhost',
//  database: 'mymonitor',
//  schema: [
//    {
//      measurement: 'nic_bandwidths',
//      fields: {
//        name: Influx.FieldType.STRING,
//        bandwidthIn: Influx.FieldType.FLOAT,
//        bandwidthOut: Influx.FieldType.FLOAT
//      },
//      tags: [
//        'host'
//      ]
//    }
//  ]
// })

// influx.writePoints([
//   {
//     measurement: 'nic_bandwidths',
//     tags: { host: 'Linux' },
//     fields: { name: 'test', bandwidthIn: 12, bandwidthOut: 3 },
//   }
// ]).then(() => {
//   // return influx.query(`
//   //   select * from nic_bandwidths
//   //   where host = ${Influx.escape.stringLit(os.hostname())}
//   //   order by time desc
//   //   limit 10
//   // `)
//   return influx.query(`
//     select * from nic_bandwidths
//     order by time desc
//     limit 10
//   `)
// }).then(rows => {
//   rows.forEach(row => console.log(`A request to ${row.path} took ${row.duration}ms`))
// })

/*
  
*/
function monitor(server, logBox) {
  var client = new nw_proto.Greeter(server + ':50051', grpc.credentials.createInsecure());
  var call = client.networkMonitor();

  call.on('data', function(mes){
    console.log('Server: ', mes.message);
    showNotification('From ' + server + ': <strong>' + mes.message + '</strong><br/>', logBox);
    if (mes.message == 'Hi') {
      console.log('Hi from server');
    } else if (mes.message.includes('icmp_seq')) {
      // manipulate output here
      let output = mes.message.split(' ');
      console.log(output);
      let delay = 0;
      if (output[6].includes('time=')) {
        addData(window.myLine, output[6].split('time=')[1]);  
      } else if (output[7].includes('time=')) {
        addData(window.myLine, output[7].split('time=')[1]);  
      }
    }
  });

  call.on('end', function() {
    console.log('End');
  }); 

  // send hi message to server after establishing connection
  call.write({message: 'Hi'});

  return call;
}


window.chartColors = [
  'rgb(255, 205, 86)',
  'rgb(75, 192, 192)',
  'rgb(54, 162, 235)',
  'rgb(153, 102, 255)'
];

function addData(chart, data) {
    // chart.data.labels.push(label);
    chart.data.datasets.forEach((dataset) => {
        dataset.data.push(data);
    });
    chart.update();
}

function setData(chains, label_seconds) {
  var dataSets = [];
  var colors = window.chartColors.concat();
  for (var c in chains) {
    var chainData = {
      label: c,
      borderColor: colors.pop(),
      fill: false,
      yAxisID: "y-axis-1",
      // steppedLine: true,
      data: chains[c]['delay'] 
    }
    dataSets.push(chainData);
  }
  var barChartData = {
    labels: label_seconds,
    datasets: dataSets
  }
  return barChartData
};

function paintChart(barChartData) {
  var ctx = document.getElementById("mChart").getContext("2d");
  // window.myBar = new Chart(ctx, {
  //     type: 'bar',
  window.myLine = new Chart(ctx, {
    type: 'line',
    data: barChartData,
    options: {
      responsive: true,
      title: {
        display: true,
        text: "Ping"
      },
      tooltips: {
        mode: 'index',
        intersect: true
      },
      scales: {
        yAxes: [{
          type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
          display: true,
          position: "left",
          id: "y-axis-1",
          // display: true,
          ticks: {
              // beginAtZero: true,
              // steps: 10,
              // stepValue: 10,
              // max: 50
            min: 0,
            // max: 10,
            // stepSize: 0.1
          },
          scaleLabel: {
            display: true,
            labelString: 'ms'
          }
        }],
        xAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'Second'
          } 
        }]
      }
    }
  });
};


function showNotification(content, logBox) {
  $('#' + logBox + ' p').append(content);
}

let server = '';
function connect() {
  $('#start-ping').click(function(){
    server = $('#server').val();
    let client = $('#client').val();
    let count = $('#count').val();
    let call = monitor(server, 'log-box');
    showNotification('Ping will start in 1s', 'log-box');
    let label_seconds = [];
    for (let i = 0; i < count; i++) {
      label_seconds.push(i);
    }
    // console.log('LABEL SECOND ' + label_seconds);
    // // console.log('COMMAND ' + cmd);
    paintChart(setData([{'delay': 0}], label_seconds));
    call.write({message: 'ping ' + client + ' -c ' + count});
  });
}

$(() => {
  connect();
})