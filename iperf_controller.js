/*
  Controller dedicated to monitor "iperf" tool:
   Iperf is a widely used tool for network performance measurement and tuning. 
   It is significant as a cross-platform tool that can produce standardized performance measurements for any network.
*/
const grpc = require('grpc');
const nw_proto = grpc.load('./proto/network_monitor.proto');


// const Influx = require('influx');
// const influx = new Influx.InfluxDB({
//  host: 'localhost',
//  database: 'mymonitor',
//  schema: [
//    {
//      measurement: 'nic_bandwidths',
//      fields: {
//        name: Influx.FieldType.STRING,
//        bandwidthIn: Influx.FieldType.FLOAT,
//        bandwidthOut: Influx.FieldType.FLOAT
//      },
//      tags: [
//        'host'
//      ]
//    }
//  ]
// })

// influx.writePoints([
//   {
//     measurement: 'nic_bandwidths',
//     tags: { host: 'Linux' },
//     fields: { name: 'test', bandwidthIn: 12, bandwidthOut: 3 },
//   }
// ]).then(() => {
//   // return influx.query(`
//   //   select * from nic_bandwidths
//   //   where host = ${Influx.escape.stringLit(os.hostname())}
//   //   order by time desc
//   //   limit 10
//   // `)
//   return influx.query(`
//     select * from nic_bandwidths
//     order by time desc
//     limit 10
//   `)
// }).then(rows => {
//   rows.forEach(row => console.log(`A request to ${row.path} took ${row.duration}ms`))
// })
function updateReport(logBox, message) {
  $('#' + logBox).empty();
  $('#' + logBox).append(message);
}

function monitor(server, logBox, buttonState) {
  var client = new nw_proto.Greeter(server + ':50051', grpc.credentials.createInsecure());
  var call = client.networkMonitor();

  call.on('data', function(mes){
    console.log('Server: ', mes.message);
    showNotification('From ' + server + ': <strong>' + mes.message + '</strong><br/>', logBox);
    if (mes.message == 'Hi') {
      $('#' + buttonState).removeClass('is-loading is-primary');
      $('#' + buttonState).text('Connected');
      $('#' + buttonState).addClass('is-success');
      $('#' + buttonState).prop('disabled', true);  
    } else if (mes.message.includes(',')) {
      // manipulate output here
      let output = mes.message.split(',');
      console.log(output);
      addData(window.myLine, output[8]/1024);
      updateReport('report', '<strong>Packet loss(%): ' + output[output.length - 2] + '<br>Bandwidth(Kbps): ' + output[8]/1024 + '<br>Jitter time(ms): ' + output[9] + '</strong>');
    }
  });

  // call.end();
  call.on('end', function() {
    console.log('End');
    // call.end();
  }); 

  call.write({message: 'Hi'});

  return call;
}

// function monitor(monitorMessage) {
//   var call = client.monitorNic();

//   let nicName = [];
//   // let seCount = 0;  // second counter
//   // let miCount = 0;  // minute counter
//   let lastUpdate;
//   call.on('data', function(mes){
//     console.log('Server: ', mes.message);
//     // let nic = new Nic()
//     let mes_split = mes.message.trim().split(/\s+/);
    
//     let noNic = 0;
//     if (mes_split[noNic] == 'Time') { // manipulate the fist message to get network interface from server
//       // console.log(mes_split[0]);
//       // let i = 1;
//       noNic = 1;
//       if (nicName.length) {
//         // if (seCount == 0 && miCount == 0) {
//         //   for (let i = 0; i < nicName.length; i++) {
//         //     nicName[i].data.save(function(err, nic){
//         //       if (err) return console.error(err);
//         //       lastUpdate = nic.time_stamp_hour;
//         //     })
//         //   }
//         // } else {
//         //   for (let i = 0; i < nicName.length; i++) {
//         //     nicName[i].data.update({'name': nicName[i].name, 'time_stamp_hour': lastUpdate}, {}, function(err, nic){

//         //     })
//         //   }
//         // }  
//         nicName = [];
//       }
//       // nicName = [];
//       while(mes_split[noNic] != 'HH:MM:SS'){
//         // nicName.push(mes_split[noNic]);
//         // let nic = new Nic({name: mes_split[noNic]});  
//         // let nicData = {'name': mes_split[noNic], 'data': nic};
//         // nicName.push(nicData);
//         nicName.push({'name': mes_split[noNic]});
//         noNic++;
//       }
//     } else { // save monitoring data to db
//       console.log('MESS ' + mes_split[noNic]);
//       for (let i = 1; i < nicName.length; i++) {
//         // for (let ii in nicName[i]) {
//         console.log('IN: ' + mes_split[i*2-1]);
//         console.log('OUT: ' + mes_split[i*2]);
//         // influx.writePoints([
//         //   {
//         //     measurement: 'nic_bandwidths',
//         //     tags: { host: 'Linux' },
//         //     fields: { name: 'test', bandwidthIn: 12, bandwidthOut: 3 },
//         //   }
//         // ]).then(() => {
//         //   // return influx.query(`
//         //   //   select * from nic_bandwidths
//         //   //   where host = ${Influx.escape.stringLit(os.hostname())}
//         //   //   order by time desc
//         //   //   limit 10
//         //   // `)
//         //   return influx.query(`
//         //     select * from nic_bandwidths
//         //     order by time desc
//         //     limit 10
//         //   `)
//         // }).then(rows => {
//         //   rows.forEach(row => console.log(`A request to ${row.path} took ${row.duration}ms`))
//         // })

//         // influx.writeMeasurement('nic_bandwidths', [
//         //   // {
//         //   //   tags: { host: 'Linux' },
//         //   //   fields: { name: nicName[i].name, bandwidthIn: mes_split[i*2-1], bandwidthOut: mes_split[i*2]},
//         //   // },
//         //   {
//         //     tags: { host: 'Linux' },
//         //     fields: { name: nicName[i].name, bandwidthIn: mes_split[i*2-1], bandwidthOut: mes_split[i*2]},
//         //   }
//         // ]).then(() => {

//         // })
//         // nicName[i].data.bandwidthIn[miCount][seCount] = mes_split[i*2-1];
//         // nicName[i].data.bandwidthOut[miCount][seCount] = mes_split[i*2];
//         // // }
//         // nicName[i].data.update({'name': nicName[i].name, 'time_stamp_hour': lastUpdate}, {bandwidthIn[miCount][seCount]: mes_split[i*2-1], bandwidthOut[miCount][seCount]: mes_split[i*2]}, function(err, nic){
//         //   if (err) return console.error(err);
//         // })
//       }
//       // seCount+=parseInt(seCount/60);
//       // seCount++;
//       // miCount+=parseInt(seCount/60);
//       // seCount = seCount % 60;
//     }
//     // console.log(nicName[0]);
//     if (mes.message == 'end') { 
//       call.end();
//     }
//   });

//   call.on('end', function(){
//     console.log('End');
//   });

//   call.write({message: 'client'});

//   // rl.on('line', function(input) {
//   //   // console.log(input);
//   //   call.write({message: input})
//   // });
//   call.write({message: monitorMessage});
//   return call;
// }
// label_hours = [];
//     // data1 = []
//     // data2 = []
//     // data3 = []
// for (var i = 0; i < 20; i++){
//   label_hours.push(i);
// }

window.chartColors = [
  'rgb(255, 205, 86)',
  'rgb(75, 192, 192)',
  'rgb(54, 162, 235)',
  'rgb(153, 102, 255)'
];

function addData(chart, data) {
    // chart.data.labels.push(label);
    chart.data.datasets.forEach((dataset) => {
        dataset.data.push(data);
    });
    chart.update();
}

function setData(chains, label_seconds) {
  var dataSets = [];
  var colors = window.chartColors.concat();
  for (var c in chains) {
    var chainData = {
      label: c,
      borderColor: colors.pop(),
      fill: false,
      yAxisID: "y-axis-1",
      // steppedLine: true,
      data: chains[c]['bandwidth'] 
    }
    dataSets.push(chainData);
  }
  var barChartData = {
    labels: label_seconds,
    datasets: dataSets
  }
  return barChartData
};

function paintChart(barChartData) {
  var ctx = document.getElementById("mChart").getContext("2d");
  // window.myBar = new Chart(ctx, {
  //     type: 'bar',
  window.myLine = new Chart(ctx, {
    type: 'line',
    data: barChartData,
    options: {
      responsive: true,
      title: {
        display: true,
        text: "Traffic Monitor"
      },
      tooltips: {
        mode: 'index',
        intersect: true
      },
      scales: {
        yAxes: [{
          type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
          display: true,
          position: "left",
          id: "y-axis-1",
          // display: true,
          ticks: {
              // beginAtZero: true,
              // steps: 10,
              // stepValue: 10,
              // max: 50
            min: 0,
            // max: 1500,
            // stepSize: 100
          },
          scaleLabel: {
            display: true,
            labelString: 'Kbps'
          }
        }],
        xAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'Second'
          } 
        }]
      }
    }
  });
};


function showNotification(content, logBox) {
  $('#' + logBox + ' p').append(content);
}

var server = '';
var client = '';

function connect() {
  $('#client-connect').click(function(){
    console.log('click client');
    $(this).addClass('is-loading');
    client = $('#client').val();
    console.log('client', client);
    let call = monitor(client, 'client-log-box', 'client-connect');

    $('button#client-tool').click(function(){
      if (server == '' || !$('#server-connect').prop('disabled')) {
        alert('Start server first!');
      } else {
        let duration = $('input#client-duration-input').val();
        let bandwidthSpeed = $('input#client-bandwidth-input').val();
        let cmd = '';
        let opt = $('input#client-opt-param').val();
        if (bandwidthSpeed == '' || bandwidthSpeed == 0) {
          cmd = 'iperf -c ' + server + ' -t ' +  duration + ' -i 1 -u ' + opt ;
        } else {
          cmd = 'iperf -c ' + server + ' -t ' +  duration + ' -i 1 -u -b ' + bandwidthSpeed + 'k ' + opt;
        }
        
        let label_seconds = [];
        for (let i = 0; i < duration; i++) {
          label_seconds.push(i);
        }
        console.log('LABEL SECOND ' + label_seconds);
        console.log('COMMAND ' + cmd);
        paintChart(setData([{'bandwidth': 0}], label_seconds));
        call.write({message: cmd});
      }
    });

    setTimeout(function(){
      var buttonState = $('#client-connect').prop('disabled');
      console.log(buttonState);
      if (!buttonState) {
        $('#client-connect').removeClass('is-loading');
        showNotification('Can not connect to <strong>' + server + '</strong><br/>', 'client-log-box');
      }
    }, 4000);
  });
  $('#server-connect').click(function(){
    console.log('click serverer');
    $(this).addClass('is-loading');
    server = $('#server').val();
    console.log('server', server);
    let call = monitor(server, 'server-log-box', 'server-connect');

    $('button#server-tool').click(function(){
      call.write({message: 'iperf -s -i 1 -y C -u ' + $('#server-opt-param').val()});
    })
    setTimeout(function(){
      var buttonState = $('#server-connect').prop('disabled');
      console.log(buttonState);
      if (!buttonState) {
        $('#server-connect').removeClass('is-loading');
        showNotification('Can not connect to <strong>' + server + '</strong><br/>', 'server-log-box');
      }
    }, 4000);
  })
}

$(() => {
  // paintChart(setData([{'bandwidth': 10000}]));
  // startChart();
  connect();
})